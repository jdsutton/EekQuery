CREATE_DATABASE_SQL = "CREATE DATABASE IF NOT EXISTS eekedout_test; USE eekedout_test;"
SUDO=$(shell which sudo)
DOCS_SOURCE=source
DOCUMENTATION=documentation

default:
	echo "Nothing to do"

install:
	$(SUDO) apt-get update
	python --version

	@$(SUDO) apt-get -y install python3-setuptools python3-dev python3.6-dev
	@git submodule update --init --recursive
	@pip install .

install_postgresql:
	@$(SUDO) apt-get -y install postgresql postgresql-server-dev-all
	@pip install -U setuptools psycopg2
	@$(SUDO) service postgresql start

install_mysql:
	@apt-cache search mariadb-server
	@echo "mariadb-server-10.0 mariadb-server-10.0/root_password password $(MYSQL_PASSWORD)" | $(SUDO) debconf-set-selections
	@echo "mariadb-server-10.0 mariadb-server-10.0/root_password_again password $(MYSQL_PASSWORD)" | $(SUDO) debconf-set-selections

	@$(SUDO) apt-get -y install mariadb-server-10.0
	@/etc/init.d/mysql start

login:
	@$(shell aws ecr get-login)

test: create_database
	python3.6 -m nose --nocapture --with-coverage --cover-package=eekquery
	coverage report -m --skip-covered

create_database:
	@echo $(CREATE_DATABASE_SQL) | mysql --host=localhost -uroot -p$(MYSQL_PASSWORD)
	@cp ./scripts/schema.sql /tmp
	@cd /tmp && $(SUDO) su postgres -c "psql -c \"CREATE DATABASE eekedout_test;\"" || true
	@cd /tmp && $(SUDO) su postgres -c "psql -d eekedout_test -c \"CREATE USER proot PASSWORD '$(MYSQL_PASSWORD)';\"" || true
	@cd /tmp && $(SUDO) su postgres -c "psql -d eekedout_test -f \"/tmp/schema.sql\""

clean:
	rm -rf build
	rm -rf *.egg-info

init_docs:
	sphinx-quickstart

docs:
	rm -rf $(DOCUMENTATION)
	mkdir $(DOCUMENTATION)
	
	# https://samnicholls.net/2016/06/15/how-to-sphinx-readthedocs/
	mkdir -p $(DOCUMENTATION)
	mkdir -p $(DOCS_SOURCE)

	sphinx-apidoc -o $(DOCS_SOURCE) ./eekquery
	sphinx-build . $(DOCUMENTATION)

	rm -rf _build
	rm -rf _static
	rm -rf _templates

create_mysql_user:
	# This monstrosity fixes the pipelines.
	@mysql -u root -p$(MYSQL_PASSWORD) -e "UPDATE mysql.user SET plugin='mysql_native_password' WHERE User='root'; UPDATE mysql.user SET password=PASSWORD('$(MYSQL_PASSWORD)') WHERE User='root'; FLUSH PRIVILEGES;"
