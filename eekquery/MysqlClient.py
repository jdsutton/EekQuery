import pymysql
from eekquery.RDBClient import RDBClient

class MysqlClient(RDBClient):
    '''
    Encapsulates logic for interacting with a Mysql database.
    '''

    def __init__(self, **kwargs):
        super().__init__()

        self._config = kwargs.get('config')
        self._database = None
        self.open()

    def open(self):
        if self._database is not None:
            self._database.ping(reconnect=True)

            return

        self._database = pymysql.connect(
            host=self._config.endpoint,
            user=self._config.username,
            passwd=self._config.password,
            db=self._config.name,
            autocommit=True,
            connect_timeout=5,
            charset=self._config.charset)
        
    def isClosed(self):
        try:
            self._database.ping(reconnect=False)
        except:
            return False

        return True

    def close(self):
        self._database.close()

    def beginTransaction(self):
        '''
        Starts a new database transaction.
        '''
        self._database.autocommit = False

    def commitTransaction(self):
        '''
        Commits the current transaction and turns autocommit back on.
        '''
        self._database.commit()
        self._database.autocommit = True

    def query(self, queryString, params=None, *, serializable=True):
        '''
        Queries the database and returns the cursor result.
        queryString: str
            An SQL query.
        params: dict
            Query params.
        '''
        if self.isClosed():
            self.open()
        
        try:
            cursor = self._database.cursor(pymysql.cursors.DictCursor)
            cursor.execute(queryString, params)
        except pymysql.err.OperationalError as e:
            self.open()
            cursor.execute(queryString, params)
            self._lastQuery = cursor._last_executed
        except BaseException as e:
            if hasattr(cursor, '_last_executed'):
                self._lastQuery = cursor._last_executed
            else:
                self._lastQuery = None
            self._logger.error('Query: {}\nParams: {}\nBuilt query: {}\nError: {}'.format(queryString, params, str(self._lastQuery), str(e)))
            raise e
            
        if serializable:
            return map(RDBClient.toJSONSerializable, cursor)
        
        return cursor
