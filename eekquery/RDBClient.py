import datetime
from decimal import Decimal
from eekquery.Row import *
from eekquery.RowHook import RowHook
from eekquery.WhereClauseBuilder import WhereClauseBuilder
import logging

class RDBClient:
    '''
    Abstract base class for relational database clients.
    '''

    _logger = logging.getLogger()
    _RowClass = Row

    class QueryError(RuntimeError):
        pass

    def __init__(self):
        self._rowHooks = []
        self._lastQuery = None

    def open(self):
        pass
        
    def isClosed(self):
        pass

    def close(self):
        pass

    def getLastQuery(self):
        return self._lastQuery

    def getLastRowId(self, cursor):
        return cursor.lastrowid

    def beginTransaction(self):
        '''
        Starts a new database transaction.
        '''

        pass

    def commitTransaction(self):
        '''
        Commits the current transaction and turns autocommit back on.
        '''

        pass

    def updateRow(self, oldRowDict, rowDict, table):
        '''
        Updates the given row in the DB.
        '''

        row = self.__class__._RowClass(rowDict)
        columns = row.getColumnNames()
        updates = row.getUpdates()
        queryParams = []
        template = ''

        for update in updates:
            template += str(update[0]) + '=%s,'
            queryParams.append(update[1])

        template = template[:len(template) - 1]

        query = 'UPDATE {} SET {}'.format(table, template)

        query += WhereClauseBuilder().createWhereClause(
            constraints=oldRowDict,
            queryParams=queryParams,
        )

        query += ';'
        
        beforeHooks = self._getHooks(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.WRITE, table)
        self._fireHooks(beforeHooks, [rowDict])

        result = self.query(query, queryParams, serializable=False)
        
        afterHooks = self._getHooks(RowHook.PREPOSITION.AFTER, RowHook.ACTION.WRITE, table)
        self._fireHooks(afterHooks, [rowDict])
        
        return result

    def writeRow(self, rowDict, table, *, overwrite=True, **kwargs):
        '''
        Writes a row to the given table.
        Updates the row if it exists (by primary key), else inserts a new row.

        rowDict - A dictionary mapping column names to values.
        table - A table from MysqlClient.TABLES.
        '''
        
        row = self.__class__._RowClass(rowDict)
        columns = row.getColumns()
        values = row.getValues()
        valuesTemplate = ','.join(['%s'] * len(values))
        updates = row.getUpdates()
        queryParams = []

        query = 'INSERT INTO {} {} VALUES ({})'.format(table, columns, valuesTemplate) 
        queryParams = queryParams + values
        
        if overwrite:
            query += ' ON DUPLICATE KEY UPDATE '
            for update in updates:
                query += update[0] + '=%s,'
                queryParams.append(update[1])
            query = query.rstrip(',')

        query += ';'
        
        beforeHooks = self._getHooks(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.WRITE, table)
        self._fireHooks(beforeHooks, [rowDict])

        result = self.query(query, queryParams, serializable=False)
        
        afterHooks = self._getHooks(RowHook.PREPOSITION.AFTER, RowHook.ACTION.WRITE, table)
        self._fireHooks(afterHooks, [rowDict])
        
        return result

    def deleteRows(self, table, constraints, entity=None, **kwargs):
        '''
        Deletes rows from the given table by column values.
        '''
        
        queryParams = []
        
        query = 'DELETE FROM {}'.format(table)
        
        builder = WhereClauseBuilder()

        query += builder.createWhereClause(constraints=constraints, queryParams=queryParams,
            entity=entity)
        
        limit = kwargs.get('limit', None)
        if limit is not None:
            query += ' LIMIT %s'
            queryParams.append(limit)

        query += ';'
        
        beforeHooks = self._getHooks(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.DELETE, table)
        afterHooks = self._getHooks(RowHook.PREPOSITION.AFTER, RowHook.ACTION.DELETE, table)
        if beforeHooks or afterHooks:
            selectQuery = query.replace('DELETE', 'SELECT *')
            result = self.query(selectQuery, queryParams)
            rowDeletions = list(result)
            self._fireHooks(beforeHooks, rowDeletions)

        result = self.query(query, queryParams)
        
        if afterHooks:
            self._fireHooks(afterHooks, rowDeletions)
        
        return result
    
    def query(self, queryString, params=None, *, serializable=True):
        '''
        Queries the database and returns the cursor result.
        queryString: str
            An SQL query.
        params: dict
            Query params.
        '''

        pass

    def getRows(self, table, constraints, *, altTableSet=None, limit=None, offset=None,
        orderBy='Id ASC', columns=['*'], groupBy=None, serializable=True):
        '''
        Returns a list of rows from table where values are equal to those given by constraints.
        '''
        
        if (limit is not None and offset is None) or (limit is None and offset is not None):
            raise RDBClient.QueryError('limit and offset must be used together.')
        
        queryParams = []

        query = 'SELECT {} FROM {}'.format(','.join(columns), table)
        
        builder = WhereClauseBuilder()
        query += builder.createWhereClause(constraints=constraints, queryParams=queryParams)

        if groupBy is not None:
            query += ' GROUP BY {}'.format(groupBy)

        if orderBy is not None:
            query += ' ORDER BY {}'.format(orderBy)

        if limit is not None and offset is not None:
            query += ' LIMIT %s OFFSET %s'
            queryParams.append(limit)
            queryParams.append(offset)

        return self.query(query, queryParams, serializable=serializable)

    @staticmethod
    def toJSONSerializable(rowDict):
        '''
        Converts a dict's fields to be JSON serializable.
        In particular, converts dates to integer timestamps,
        and converts Decimal to float.
        '''
        result = dict()

        for field, _ in rowDict.items():
            if type(rowDict[field]) == datetime.date:
                result[field] = str(rowDict[field])
            elif isinstance(rowDict[field], datetime.datetime):
                # http://www.psf.upfronthosting.co.za/issue31212
                try:
                    result[field] = int(rowDict[field].timestamp() * 1000)
                except ValueError:
                    result[field] = None
            elif type(rowDict[field]) == Decimal:
                result[field] = float(rowDict[field])
            else:
                result[field] = rowDict[field]

        return result

    def getRow(self, *args, **kwargs):
        '''
        Returns a single row, or None if now row is found.
        Has identical parameters to getRows(),
        '''

        kwargs['limit'] = 1

        if kwargs.get('offset', None) is None:
            kwargs['offset'] = 0

        result = self.getRows(*args, **kwargs)

        try:
            return list(result)[0]
        except IndexError:
            return None

    def addRowHook(self, hook):
        '''
        Add a row hook that will be fired as necessary.
        '''
        
        self._rowHooks.append(hook)

        return self
    
    def _getHooks(self, preposition, action, tableName=None):
        function = lambda hook: \
            preposition == hook.preposition \
            and action == hook.action \
            and (tableName is None or tableName == hook.tableName)
        return list(filter(function, self._rowHooks))
    
    def _fireHooks(self, hooks, rows):
        for hook in hooks:
            for row in rows:
                hook.callback(row) 
 