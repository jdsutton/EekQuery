from collections import defaultdict
from eekquery.Constraint import Constraint

class SmartTable:
    '''
    Reads and writes self.__class__.dataTypes to/from the DB.
    '''
    
    dataType = None

    class InvalidPermissionException(RuntimeError):
        def __init__(self, permission):
            super().__init__(str(permission))

    def __init__(self, dbClient, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._dbClient = dbClient
        self._permissionsByMethodName = defaultdict(list)

    def addPermission(self, methodName, permission):
        '''
        Adds a permission required to execute the given method.
        Permission should be a callable returning True if the method is permitted.
        '''

        self._permissionsByMethodName[methodName].append(permission)

    def write(self, dataObject, **kwargs):
        '''
        Writes a self.__class__.dataType.
        '''

        if not isinstance(dataObject, list):
            dataObject = [dataObject]

        obj = None

        if not len(dataObject):
            return None

        for obj in dataObject:
            for permission in self._permissionsByMethodName['write']:
                self._assertPermission(permission, dataObject, **kwargs)

            if not isinstance(obj, self.__class__.dataType):
                obj = self.__class__.dataType(obj)
            
            cursor = obj.write(self._dbClient)
        
            newId = self._dbClient.getLastRowId(cursor)

            if newId:
                obj.id = newId

        return obj

    def _assertPermission(self, permission, *args, **kwargs):
        allowed = permission(*args, **kwargs)

        if not allowed:
            raise SmartTable.InvalidPermissionException(permission)

    def get(self, dataObjectId, **kwargs):
        '''
        Fetches a self.__class__.dataType.
        '''

        for permission in self._permissionsByMethodName['get']:
            self._assertPermission(permission, dataObjectId, **kwargs)

        return self.__class__.dataType({
            'id': dataObjectId,
        }).read(self._dbClient)

    def delete(self, dataObject, **kwargs):
        '''
        Deletes a self.__class__.dataType.
        '''

        for permission in self._permissionsByMethodName['delete']:
            self._assertPermission(permission, dataObject, **kwargs)

        self.__class__.dataType(dataObject).delete(self._dbClient)

    def list(self, *args, limit, offset, orderBy=None, query=None, queryColumn=None, constraints=[],
        **kwargs):
        '''
        Lists self.__class__.dataTypes.
        '''

        for permission in self._permissionsByMethodName['list']:
            self._assertPermission(permission, *args, **kwargs)

        # TODO: Test for SQL injection vulnerability.
        # for i in range(len(constraints)):
        #     c = constraints[i]

        #     constraints[i] = Constraint(
        #         targetColumnName=c.get('targetColumnName', None),
        #         comparisonOperator=c.get('comparisonOperator', None),
        #         operand=c.get('operand', None),
        #     )

        return self.__class__.dataType.query(
            self._dbClient,
            query=query,
            queryColumn=queryColumn,
            addedConstraints=constraints,
            limit=limit,
            offset=offset,
            orderBy=orderBy,
        )
