from collections import defaultdict
from eekquery.Constraint import Constraint
from eekquery.TrustedStringLiteral import TrustedStringLiteral
import json

class DataObject:
    '''
    An object carrying data to/from the database.
    Handles set/unset/None fields, optional/required fields.
    '''

    # Table to read/write to/from.
    TABLE = None

    class JSONEncoder(json.JSONEncoder):

        def default(self, obj):
            if isinstance(obj, DataObject):
                return obj.asDict()

            return super().default(obj)

    def __init__(self, fromDict, *, autoField=False):
        self._data = dict() # Buffer data until fields are created.
        self._fields = set()
        self._keys = set()
        self._keysNotToWrite = set()
        self._currentTableName = self.__class__.TABLE
        self._fieldsByTableName = defaultdict(list)

        for key, value in fromDict.items():
            self._data[key] = value

            if autoField:
                self.addField(key)
                self._setValueOnSelf(key, value)

    def __str__(self):
        return str(self.asDict())

    def __repr__(self):
        return self.__str__()

    def _setValueOnSelf(self, key, value):
        '''
        This exists to be overwritten for purposes of identifier transformation.
        '''

        setattr(self, key, value)

    def hasField(self, field):
        return field in self._fields

    @classmethod
    def fromJoinedRow(cls, row, prefix):
        result = dict()

        if not prefix.endswith('.'):
            prefix = prefix + '.'

        for key in row:
            if key.startswith(prefix):
                result[key.replace(prefix, '', 1)] = row[key]

        return cls(result, autoField=True)

    def get(self, propName, default):
        return self.__dict__.get(propName, default)

    def copy(self):
        return self.__class__(self)

    def items(self):
        '''
        Iterates over the keys and values in the object's data.
        '''

        return self.asDict().items()

    def hasPropertyNotNone(self, key):
        '''
        Returns true if a value has been assigned for the given key and the value is not None.
        '''

        return hasattr(self, key) and getattr(self, key) is not None

    def forTable(self, tableName):
        '''
        Defines the table which the following field definitions will be for.
        '''

        self._currentTableName = tableName

        return self

    def addField(self, key, *, required=False, isKey=False, write=True):
        '''
        Dynamically adds a field to the instance.
        '''

        if self.get('_currentTableName', None) is None:
            self._currentTableName = self.__class__.TABLE

        self._fieldsByTableName[self._currentTableName].append(key)

        self._fields.add(key)

        if isKey:
            self._keys.add(key)

        if not write:
            self._keysNotToWrite.add(key)

        if key in self._data:
            self._setValueOnSelf(key, self._data[key])
        elif required:
            raise RuntimeError('Required value missing for key: {}'.format(key))

        return self

    def asTableRow(self, tableName=None):
        '''
        Returns a dict which may be written as a row to the given table.
        '''

        if tableName is None:
            tableName = self._currentTableName
        
        fields = self._fieldsByTableName[tableName]
        result = dict()

        for field in fields:
            if hasattr(self, field) and not field in self._keysNotToWrite:
                result[field] = getattr(self, field)

        return result

    def asDict(self):
        result = dict()

        for key in self._fields:
            if hasattr(self, key):
                result[key] = getattr(self, key)
        
        return result

    def getKeys(self, checkRequired=True):
        keys = dict()

        for key in self._keys:
            if hasattr(self, key):
                keys[key] = getattr(self, key)

        return keys

    def listKeys(self):
        return list(self.getKeys().keys())

    def read(self, dbClient, *, byKeysOnly=True):
        '''
        Reads the object from the DB.
        '''

        if byKeysOnly:
            keys = self.getKeys()
        else:
            keys = self.asTableRow()

        result = dbClient.getRow(self.__class__.TABLE, keys,
            offset=0, limit=1, orderBy=None)

        if result is None:
            return None

        return self.__class__(result)

    def write(self, dbClient):
        '''
        Writes the object to the DB.
        '''
        
        row = self.asTableRow(self.__class__.TABLE)
        keys = list(self.getKeys().keys())

        if self.hasField('id'):
            returning = 'id'
        else:
            returning = None

        return dbClient.writeRow(row, self.__class__.TABLE, keys=keys, returning=None)

    def update(self, dbClient, onlyFields=None):
        '''
        Updates the object in the DB.
        '''
        
        if onlyFields:
            row = self.getKeys()

            for field in onlyFields:
                row[field] = self.get(field, None)
        else:
            row = self.asTableRow(self.__class__.TABLE)

        return dbClient.updateRow(self.getKeys(), row, self.__class__.TABLE)

    def delete(self, dbClient, *, byKeysOnly=True):
        '''
        Deletes the object from the database.
        '''

        if byKeysOnly:
            keys = self.getKeys()
        else:
            keys = self.asTableRow()

        result = dbClient.deleteRows(self.__class__.TABLE, keys)

    @classmethod
    def query(cls, dbClient, *, query=None, queryColumn=None, addedConstraints=None,
        limit=None, offset=None, table=None, orderBy=None):
        '''
        Searches for rows of this class by string matching.
        '''

        constraints = []

        if query is not None and queryColumn is not None:
            queryConstraint = Constraint(
                targetColumnName=queryColumn,
                comparisonOperator='LIKE',
                operand=TrustedStringLiteral('CONCAT("%%",%s,"%%")'),
                arguments=[query],
            )
            constraints.append(queryConstraint)

        if addedConstraints is not None:
            constraints += addedConstraints

        if table is None:
            table = cls.TABLE

        result = dbClient.getRows(table, constraints, limit=limit,
            offset=offset, orderBy=orderBy)

        return map(cls, result)
