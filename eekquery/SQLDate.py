from datetime import datetime, timezone
import iso8601

class SQLDate:
    '''
    Handles dates for writing in SQL.
    '''

    SQL_FORMAT = '%Y-%m-%d %H:%M:%S'

    def __init__(self, dt=None):
        if dt is None:
            self._datetime = datetime.utcnow()
        elif isinstance(dt, datetime):
            self._datetime = dt
        elif isinstance(dt, SQLDate):
            self._datetime = dt._datetime
        elif isinstance(dt, str):
            self._datetime = iso8601.parse_date(dt)
        else:
            try:
                timestamp = int(dt)
                self._datetime = datetime.fromtimestamp(timestamp / 1000)
            except:
                raise RuntimeError('Invalid input: {}'.format(dt))

        self._datetime = self._datetime.replace(tzinfo=timezone.utc)

    def get(self):
        return self._datetime

    def __str__(self):
        return self._datetime.strftime(SQLDate.SQL_FORMAT)

    def __lt__(self, other):
        return self._datetime < (other._datetime if isinstance(other, SQLDate) else other)

    def __gt__(self, other):
        return self._datetime > (other._datetime if isinstance(other, SQLDate) else other)

    def __add__(self, other):
        return self._datetime + other

    def __sub__(self, other):
        return self._datetime - other

    @staticmethod
    def getCurrentTimeInMilliseconds():
        '''
        Returns the current time in milliseconds since the epoch.
        '''

        return int(datetime.utcnow().timestamp() * 1000)
