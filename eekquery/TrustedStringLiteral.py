class TrustedStringLiteral:
    '''
    String to literally paste into an SQL query.
    '''

    def __init__(self, stringLiteral, quote=False):
        '''
        Initialize this TrustedStringLiteral.
        
        quote - Whether or not to wrap `stringLiteral` in quotes.
        '''
        
        self._stringLiteral = stringLiteral
        self.quote = quote
        
    def __str__(self):
        return '"{}"'.format(self._stringLiteral) if self.quote else self._stringLiteral