from collections import defaultdict 
import json

class MockDBClient:

    _FILE_LOCATION = 'data.mockdb'

    _nextId = 1

    def __init__(self, *args, **kwargs):
        self._tables = defaultdict(dict)

        self._load()

    @classmethod
    def getNextId(cls):
        result = cls._nextId

        cls._nextId += 1

        return result

    def _load(self):
        try:
            with open(MockDBClient._FILE_LOCATION, 'r') as f:
                s = f.read()
                data = json.loads(s)

                for key, value in data.items():
                    self._tables[key] = value
        except FileNotFoundError:
            pass

    def _save(self):
        try:
            data = json.dumps(self._tables, default=lambda x: str(x))
        except TypeError:
            data = str

        with open(MockDBClient._FILE_LOCATION, 'w') as f:
            f.write(data)

    def open(self):
        pass
        
    def isClosed(self):
        pass

    def close(self):
        pass

    def writeRow(self, rowDict, table, **kwargs):
        table = self._tables[table]
        d = dict()

        rowId = rowDict.get('id', MockDBClient.getNextId())

        if rowId in table:
            old = table[rowId]

            for key, value in rowDict.items():
                old[key] = value
        else:
            table[rowId] = rowDict
        
        self._save()

        return rowId

    def batchUpdate(self, updates, table, **kwargs):
        pass

    def getRow(self, *args, **kwargs):
        result = self.getRows(*args, **kwargs)

        try:
            return result[0]
        except IndexError:
            return None

    def getDocument(self, table, docId):
        return self._tables[table].get(docId, None)

    def doTransaction(self, table, doc, deltaF):
        snap = self.getDocument(table, doc)
        update = deltaF(snap)

        del self._tables[table][doc]

        self.writeRow(update, table)

        return True

    @staticmethod
    def _matchesConstraints(row, constraints):
        if isinstance(constraints, list):
            newConstraints = {}

            for constraint in constraints:
                newConstraints[constraint.targetColumnName] = (
                    constraint.comparisonOperator,
                    constraint.operand,
                )

            constraints = newConstraints

        for key, value in constraints.items():
            if not key in row:
                return False

            if isinstance(value, tuple):
                rowValue = row[key]
                operator = value[0]
                operand = value[1]

                if operator == '!=':
                    if rowValue == operand:
                        return False
                elif operator == '>=':
                    if rowValue < operand:
                        return False
                else:
                    raise RuntimeError('Operator "{}" not supported'.format(operator))

            elif row[key] != value:
                return False

        return True

    def getRows(self, table, constraints, **kwargs):
        assert(table is not None and len(table) > 0)

        contents = list(self._tables[table].values())
        result = []

        for row in contents:

            if MockDBClient._matchesConstraints(row, constraints):
                result.insert(0, row.copy())

        return result

    def deleteRows(self, table, constraints, **kwargs):
        assert(table is not None and len(table) > 0)

        keys = list(self._tables[table].keys())

        for rowId in keys:
            row = self._tables[table][rowId]

            if MockDBClient._matchesConstraints(row, constraints):
                del self._tables[table][rowId]

        self._save()
