from .MockDBClient import MockDBClient


class MockDBClientFactory:

    _client = MockDBClient()

    @classmethod
    def produce(cls, *args, **kwargs):
        return cls._client