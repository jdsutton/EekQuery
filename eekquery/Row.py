
from eekquery.DataObject import DataObject

class Row:
    '''
    Generic class representing a row in a table.
    '''
    
    def __init__(self, row):
        if isinstance(row, Row):
            self._rowDict = row._rowDict
        elif isinstance(row, DataObject):
            self._rowDict = row.asTableRow()
        else:
            self._rowDict = row

    def getByColumnName(self, col):
        '''
        Returns the value in the given column for this row.
        '''
        return self._rowDict[col]          

    def getValues(self):
        '''
        Returns a list of values for each column in the row.
        '''
        return list(self._rowDict.values())


    def getColumnNames(self):
        return self._rowDict.keys()

    def getColumns(self):
        '''
        Returns a string description of this row's columns.
        Used for building SQL queries.
        '''
        commaSeparatedKeys = ','.join(map(
            lambda x: '`{}`'.format(x),
            self._rowDict.keys(),
        ))
        
        return '({})'.format(commaSeparatedKeys)  

    def getUpdates(self):
        '''
        Returns a list of (column name, value) pairs.
        '''

        result = []

        for key, value in self._rowDict.items():
            result.append(('`{}`'.format(key), value))

        return result
