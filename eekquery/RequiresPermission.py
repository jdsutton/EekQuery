class RequiresPermission:
    '''
    Locks down a method using some permission.
    '''

    class PermissionException(RuntimeError):
        pass

    def __init__(self, permissionF):
        self._permissionF = permissionF

    def __call__(self, f):

        def g(*args, **kwargs):
            if not self._permissionF(*args, **kwargs):
                raise RequiresPermission.PermissionException('Function call not permitted. Permission check failed: ' + str(self._permissionF))

            return f(*args, **kwargs)

        return g
