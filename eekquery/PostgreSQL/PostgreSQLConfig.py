class PostgreSQLConfig:
    '''
    Configuration for a PostgreSQLClient.
    '''

    def __init__(self, data):
        self.username = data['username']
        self.password = data['password']
        self.name = data['name']
        self.endpoint = data['endpoint']
        self.port = data.get('port', 5432)
