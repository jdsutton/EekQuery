from eekquery.DataObject import DataObject
from eekquery.RDBClient import RDBClient
from eekquery.PostgreSQL.PostgreSQLConfig import PostgreSQLConfig
from eekquery.PostgreSQL.PostgreSQLRow import PostgreSQLRow
from eekquery.RowHook import RowHook
import logging
import psycopg2
from psycopg2.extras import DictCursor
from psycopg2.errors import UniqueViolation

from EekQuery.eekquery.PostgreSQL.PostgreSQLConfig import PostgreSQLConfig

class PostgreSQLClient(RDBClient):
    '''
    Encapsulates logic for interacting with a PostgreSQL database.
    '''

    _RowClass = PostgreSQLRow
    CONFIG_CLASS = PostgreSQLConfig

    def __init__(self, config, **kwargs):
        super().__init__()

        self._config = config
        self._connection = None
        self._autoCommit = True

    def open(self):
        self._connection = psycopg2.connect(
            dbname=self._config.name,
            user=self._config.username,
            password=self._config.password,
            host=self._config.endpoint,
            port=self._config.port,
        ) 
        
    def isClosed(self):
        raise RuntimeError('Operation not supported')

    def close(self):
        self._connection.close()

    def beginTransaction(self):
        '''
        Starts a new database transaction.
        '''

        self._autoCommit = False

    def commitTransaction(self):
        '''
        Commits the current transaction and turns autocommit back on.
        '''

        self._connection.commit()
        self._autoCommit = True

    def getLastRowId(self, cursor):
        try:
            return cursor.fetchone()[0]
        except:
            return None

    @staticmethod
    def _quoteKeys(data):
        result = dict()

        for key, value in data.items():
            newKey = '"{}"'.format(key)
            result[newKey] = value

        return result

    @staticmethod
    def _quoteConstraints(constraints):
        if isinstance(constraints, dict):
            return PostgreSQLClient._quoteKeys(constraints)

        for c in constraints:
            c.targetColumnName = '"{}"'.format(c.targetColumnName)

        return constraints

    def getRows(self, table, constraints, *args, orderBy=None, **kwargs):
        constraints = PostgreSQLClient._quoteConstraints(constraints)

        if orderBy is not None:
            parts = orderBy.split(',')

            for i in range(len(parts)):
                columnName, order = parts[i].split(' ')

                parts[i] = '"{}" {}'.format(columnName, order)

            orderBy = ','.join(parts)

        return super().getRows('{}'.format(table), constraints, *args, orderBy=orderBy, **kwargs)

    def deleteRows(self, table, constraints, *args, **kwargs):
        constraints = PostgreSQLClient._quoteConstraints(constraints)

        return super().deleteRows(table, constraints, *args, **kwargs)


    def writeRow(self, rowDict, table, *, returning='id', overwrite=True, keys=[], **kwargs):
        '''
        Writes a row to the given table.
        Updates the row if it exists (by primary key), else inserts a new row.

        rowDict - A dictionary mapping column names to values.
        table - A table from MysqlClient.TABLES.
        '''

        assert(type(keys) is list)
        
        row = self.__class__._RowClass(rowDict)
        columns = row.getColumns()
        values = row.getValues()
        valuesTemplate = ','.join(['%s'] * len(values))
        updates = row.getUpdates()
        queryParams = []

        query = 'INSERT INTO {} {} VALUES ({})'.format(table, columns, valuesTemplate) 
        queryParams = queryParams + values

        if overwrite and len(keys) > 0:
            query += ' ON CONFLICT ({}) DO UPDATE SET '.format(
                str(['"{}"'.format(key) for key in keys])[1:-1].replace('\'', '')
            )
            for update in updates:
                query += update[0] + '=%s,'
                queryParams.append(update[1])
            query = query.rstrip(',')
        else:
            query += ' ON CONFLICT DO NOTHING'

        if returning is not None:
            query = query + ' RETURNING ' + returning

        query += ';'
        
        beforeHooks = self._getHooks(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.WRITE, table)
        self._fireHooks(beforeHooks, [rowDict])

        result = self.query(query, queryParams, serializable=False)
        
        afterHooks = self._getHooks(RowHook.PREPOSITION.AFTER, RowHook.ACTION.WRITE, table)
        self._fireHooks(afterHooks, [rowDict])
        
        return result

    def writeRows(self, rows, table, *, keys=None, overwrite=True, **kwargs):
        '''
        Writes rowsto the given table.
        Updates the row if it exists (by primary key), else inserts a new row.

        rowDict - A dictionary mapping column names to values.
        table - A table from MysqlClient.TABLES.
        '''

        if keys is None:
            row = rows[0]
            if isinstance(row, DataObject):
                keys = row.listKeys()
        else:
            keys = []

        for i in range(len(rows)):
            rows[i] = self.__class__._RowClass(rows[i])
        
        columns = rows[0].getColumns()
        queryParams = []

        query = 'INSERT INTO {} {} VALUES'.format(table, columns) 

        for row in rows:
            values = row.getValues()
            query += ' ({})'.format(','.join(['%s'] * len(values)))
            
            if row is not rows[-1]:
                query += ','

            queryParams = queryParams + values

        if overwrite:
            if not len(keys):
                raise RuntimeError('Must call with keys=... or pass DataObjects with defined keys.')

            query += ' ON CONFLICT ({}) DO UPDATE SET '.format(
                str(['"{}"'.format(key) for key in keys])[1:-1].replace('\'', '')
            )
            for update in rows[0].getUpdates():
                query += update[0] + '=EXCLUDED.{},'.format(update[0])
            query = query.rstrip(',')
        else:
            query += ' ON CONFLICT DO NOTHING'
        
        beforeHooks = self._getHooks(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.WRITE, table)
        self._fireHooks(beforeHooks, [rows])

        result = self.query(query, queryParams, serializable=False)
        
        afterHooks = self._getHooks(RowHook.PREPOSITION.AFTER, RowHook.ACTION.WRITE, table)
        self._fireHooks(afterHooks, [rows])

        return result
    
    def query(self, queryString, params=None, *, serializable=True):
        '''
        Queries the database and returns the cursor result.
        queryString: str
            An SQL query.
        params: dict
            Query params.
        '''

        if self._connection is None:
            self.open()

        queryString = queryString.replace('`', '')
        cursor = self._connection.cursor(cursor_factory=DictCursor)
        result = []

        try:
            cursor.execute(queryString, params)
            self._lastQuery = cursor.query

            # TODO: This could be better.
            if queryString.strip().startswith('SELECT'):
                result = cursor.fetchall()
        except psycopg2.OperationalError as e:
            self.open()
            cursor.execute(queryString, params)
            self._lastQuery = cursor.query

            # TODO: This could be better.
            if queryString.strip().startswith('SELECT'):
                result = cursor.fetchall()
        except BaseException as e:
            if hasattr(cursor, 'query'):
                self._lastQuery = cursor.query
            else:
                self._lastQuery = None

            self._connection.rollback()
            
            self._logger.error('Query: {}\nParams: {}\nBuilt query: {}\nError: {}'.format(
                queryString,
                params,
                str(self._lastQuery),
                str(e),
            ))
            
            raise e

        if isinstance(self._lastQuery, bytes):
            self._lastQuery = self._lastQuery.decode()

        if self._autoCommit:
            self.commitTransaction()
            
        if serializable:
            return map(RDBClient.toJSONSerializable, result)
        
        return cursor
