from eekquery.Row import Row

class PostgreSQLRow(Row):

    def getColumnNames(self):
        return [
            '"{}"'.format(key)
            for key in self._rowDict.keys()
        ]

    def getColumns(self):
        '''
        Returns a string description of this row's columns.
        Used for building SQL queries.
        '''
        commaSeparatedKeys = ','.join(map(
            lambda x: '"{}"'.format(x),
            self._rowDict.keys(),
        ))
        
        return '({})'.format(commaSeparatedKeys)  

    def getUpdates(self):
        '''
        Returns a list of (column name, value) pairs.
        '''

        result = []

        for key, value in self._rowDict.items():
            result.append(('"{}"'.format(key), value))

        return result