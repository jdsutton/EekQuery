from collections import defaultdict
from eekquery.MysqlClient import MysqlClient
from eekquery.MysqlConfig import MysqlConfig
from eekquery.PostgreSQL.PostgreSQLClient import PostgreSQLClient
from eekquery.RowHook import RowHook
import os

class RDBClientFactory:
    '''
    Produces RDBClients
    '''

    _clientsByPIDAndName = defaultdict(dict)

    @classmethod
    def produce(cls, config, *, clientClass=PostgreSQLClient):
        '''
        Returns a pre-configured MysqlClient.
        '''

        pid = os.getpid()

        if pid in cls._clientsByPIDAndName:
            clients = cls._clientsByPIDAndName[pid]
            
            if config.dbName in clients:
                return clients[config.dbName]

        dbConfig = MysqlConfig(
            config.dbUsername,
            config.dbPassword,
            config.dbName,
            config.dbEndpoint,
            charset=config.get('dbCharset', None),
            port=config.get('dbPort', 3306)
        )

        client = clientClass(config=dbConfig) 

        cls._clientsByPIDAndName[pid][config.dbName] = client

        return client
