from enum import Enum

class RowHook:
    '''
    Hook to fire on some row action.
    '''
    
    class PREPOSITION(Enum):
        BEFORE = 0
        AFTER = 1
        
    class ACTION(Enum):
        DELETE = 0
        WRITE = 1

    def __init__(self, preposition, action, tableName, callback):
        self.preposition = preposition
        self.action = action
        self.tableName = tableName
        self.callback = callback
