from eekquery.DataObject import DataObject
import json
import os

class ConfigFactory:
    '''
    Returns configuration details.
    '''

    class ENV:
        TEST = 'test'
        DEV = 'dev'
        PROD = ''

    _env = ENV.DEV

    @classmethod
    def produce(cls, configFile):
        configFile = os.path.join(os.getcwd(), configFile)

        with open(configFile) as f:
            lines = f.read()

        config = DataObject(json.loads(lines), autoField=True)

        return config

    @classmethod
    def setEnvironment(cls, env):
        cls._env = env

        return cls
