from eekquery.MysqlClient import MysqlClient
from eekquery.RDBClientFactory import RDBClientFactory

class MysqlClientFactory(RDBClientFactory):
    '''
    Produces MysqlClients
    '''

    @classmethod
    def produce(cls, config):
        '''
        Returns a pre-configured MysqlClient.
        '''

        
        return super().produce(config, clientClass=MysqlClient)
