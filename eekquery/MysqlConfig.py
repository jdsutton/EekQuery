from os import environ

class MysqlConfig:
    '''
    Configuration for a MysqlClient.
    '''

    def __init__(self, username, password, name, endpoint, *, charset='latin1', port=3306):
        self.username = username
        self.password = password
        self.name = name
        self.endpoint = endpoint
        self.port = port
        self.charset = charset

TEST_CONFIG = MysqlConfig('root', environ.get('MYSQL_PASSWORD'), 'eekedout_test', '127.0.0.1')