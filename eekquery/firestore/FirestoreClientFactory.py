from collections import defaultdict
import os
from .FirestoreClient import FirestoreClient

class FirestoreClientFactory:
    '''
    Produces FirestoreClients
    '''

    _clientsByPID = defaultdict(dict)

    @classmethod
    def produce(cls, config):
        '''
        Returns a pre-configured FirestoreClient.
        '''

        pid = os.getpid()

        if pid in cls._clientsByPID:
            return cls._clientsByPID[pid]

        client = FirestoreClient(config) 

        cls._clientsByPID[pid] = client

        return client
