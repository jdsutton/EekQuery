from eekquery.RDBClient import RDBClient
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from google.cloud.firestore_v1 import Increment
from uuid import uuid4

class FirestoreClient(RDBClient):
    '''
    Firestore database client.
    '''

    _ORDER_DIRECTION = {
        'DESC': firebase_admin.firestore.Query.DESCENDING,
        'ASC': firebase_admin.firestore.Query.ASCENDING,
    }

    _MAX_BATCH_SIZE = 500

    def __init__(self, config):
        cred = credentials.Certificate(config.serviceAccountPath)
        firebase_admin.initialize_app(cred)

        self._db = firestore.client()

    def getFirestore(self):
        return self._db

    def listDocuments(self, table):
        return self._db.collection(table).list_documents()

    def open(self):
        pass
        
    def isClosed(self):
        pass

    def close(self):
        pass

    def writeRow(self, rowDict, table, **kwargs):
        docId = rowDict.get('id', None)

        reference = self._db.collection(table)

        if not docId:
            # Temporary fix.
            # https://github.com/googleapis/python-firestore/issues
            docId = str(uuid4())

            reference = reference.document(docId)
        else:
            reference = reference.document(docId)
            
        reference.set(rowDict, merge=True)

        return reference.id

    def batchUpdate(self, updates, table):
        if len(updates) > FirestoreClient._MAX_BATCH_SIZE:
            raise RuntimeError('Too many updates')

        batch = self._db.batch()
        reference = self._db.collection(table)

        for update in updates:
            doc = reference.document(update['id'])

            batch.update(doc, update)

        batch.commit()

    @staticmethod
    def _cleanRow(row):
        result = row.to_dict()

        if result is None:
            return None
        
        result['id'] = row.id

        return result

    def getDocument(self, table, docId):
        reference = self._db.collection(table).document(docId)

        result = reference.get()

        if result is None:
            return None

        return FirestoreClient._cleanRow(result)

    def getRows(self, table, constraints, *, orderBy=None, limit=None, startAfterId=None, **kwargs):
        reference = self._db.collection(table)
        
        if isinstance(constraints, dict):
            for key, value in constraints.items():
                if isinstance(value, tuple):
                    reference = reference.where(key, value[0], value[1])
                else:
                    reference = reference.where(key, '==', value)

        #  Handle list of Constraint.
        if isinstance(constraints, list):
            for constraint in constraints:
                operator = constraint.comparisonOperator
                
                if operator is '=':
                    operator = '=='

                reference = reference.where(
                    constraint.targetColumnName,
                    operator,
                    constraint.operand,
                )

        if startAfterId is not None:
            snapshot = self._db.collection(table).document(startAfterId).get()

            reference = reference.start_after(snapshot)

        if orderBy is not None:
            key, order = orderBy.split(' ')
            order = FirestoreClient._ORDER_DIRECTION[order]
            
            reference = reference.order_by(key, direction=order)

        if limit is not None:
            reference = reference.limit(limit)

        return map(FirestoreClient._cleanRow, reference.stream())

    def deleteById(self, table, docId):
        self._db.collection(table).document(docId).delete()

    def deleteRows(self, table, constraints, **kwargs):
        reference = self._db.collection(table)
        batch = self._db.batch()

        for key, value in constraints.items():
            reference = reference.where(key, '==', value)

        for snapshot in reference.stream():
            doc = self._db.collection(table).document(snapshot.id)
            batch.delete(doc)

        batch.commit()

    def increment(self, table, doc, field, incrementBy):
        doc = self._db.collection(table).document(doc)

        update = {}
        update[field] = Increment(incrementBy)
        
        doc.update(update)

    def doTransaction(self, table, doc, deltaF):
        transaction = self._db.transaction()
        ref = self._db.collection(table).document(doc)

        @firestore.transactional
        def update(transaction, ref):
            snap = ref.get(transaction=transaction).to_dict()

            update = deltaF(snap)

            if update is not False:
                transaction.update(ref, update)

            return self._db.collection(table).document(doc)

        return update(transaction, ref)
