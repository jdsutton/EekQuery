from eekquery.test.MockDBClient import MockDBClient
import unittest

class test_MockDBClient(unittest.TestCase):
    
    def test(self):
        db = MockDBClient()
        table = 'fake-table'

        row = {
            'a': 1234,
        }

        db.writeRow(row, table)

        result = db.getRow(table, row)

        self.assertEqual(result['a'], row['a'])

if __name__ == '__main__':
    unittest.main()