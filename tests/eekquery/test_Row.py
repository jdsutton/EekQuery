from eekquery.Row import Row
import unittest

class test_Row(unittest.TestCase):
    
    def setUp(self):
        global row

        row = Row({
            'Col1': 1,
            'Col2': 2
        })

        row2 = Row(row)

    def test_getColumns(self):
        cols = sorted(row.getColumns()[1:-1].split(','))
        self.assertEqual(cols[0], '`Col1`')
        self.assertEqual(cols[1], '`Col2`')

    def testGetByColumnName(self):
        self.assertEqual(row.getByColumnName('Col1'), 1)
        self.assertEqual(row.getByColumnName('Col2'), 2)

    def test_getValues(self):
        values = sorted(row.getValues())
        self.assertEqual(values[0], 1)
        self.assertEqual(values[1], 2)

if __name__ == '__main__':
    unittest.main()
