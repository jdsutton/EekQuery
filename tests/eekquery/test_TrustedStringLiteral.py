from eekquery.TrustedStringLiteral import TrustedStringLiteral
import unittest

class test_TrustedStringLiteral(unittest.TestCase):
    
    def test__str__(self):
        expected = 'Foo'
        literal = TrustedStringLiteral('Foo')
        actual = str(literal)
        self.assertEqual(expected, actual)
        
        expected = '"Foo"'
        quote = True
        literal = TrustedStringLiteral('Foo', quote)
        actual = str(literal)
        self.assertEqual(expected, actual)

if __name__ == '__main__':
    unittest.main()
