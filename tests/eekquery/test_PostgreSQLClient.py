from copy import copy
from datetime import datetime
from eekquery.PostgreSQL.PostgreSQLClient import PostgreSQLClient
from eekquery.MysqlConfig import TEST_CONFIG
from eekquery.RowHook import RowHook
import json
from psycopg2.errors import UniqueViolation
import unittest

class test_PostgreSQLClient(unittest.TestCase):

    class Package:

        def __init__(self):
            pass
    
    def setUp(self):
        global dbClient

        config = copy(TEST_CONFIG)
        config.username = 'proot'
        config.port = 5432

        dbClient = PostgreSQLClient(config=config)
        dbClient.query('DELETE FROM packages;')
        dbClient.query('''
            INSERT INTO packages ("id", "FacilityNumber", "product_name") VALUES
            (1, 'FacilityNumber0', 'cool product'),
            (2, 'FacilityNumber0', 'cool product'),
            (3, 'FacilityNumber0', 'cool product'),
            (4, 'FacilityNumber0', 'cool product'),
            (5, 'FacilityNumber1', 'cool product'),
            (6, 'FacilityNumber1', 'cool product'),
            (7, 'FacilityNumber1', 'cool product');
        ''')

    def test_toJSONSerializable(self):
        packages = dbClient.query('SELECT * FROM packages;')
        packages = map(PostgreSQLClient.toJSONSerializable, packages)

        jsonSerialized = json.dumps(list(packages))

        class BadDateTime(datetime):

            def timestamp(self):
                raise ValueError()

        packages = [{
            'date': BadDateTime(1, 1, 1)
        }]
        packages = map(PostgreSQLClient.toJSONSerializable, packages)
        jsonSerialized = json.dumps(list(packages))

    def test_writeRow(self):
        packageRow = {
            'id': 1234567,
            'FacilityNumber': 'FakeFacilityNumber, LLC',
            'product_name': 'Cool Product'
        }

        result = dbClient.writeRow(packageRow, 'packages')
        self.assertIsNotNone(result.lastrowid)

        def alreadyExists():
            dbClient.writeRow(packageRow, 'packages', overwrite=False)

        self.assertRaises(UniqueViolation, alreadyExists)

        packages = dbClient.query('SELECT * FROM packages WHERE "FacilityNumber"=\'{}\';'.format('FakeFacilityNumber, LLC'))
        packages = list(packages)
        self.assertEqual(len(packages), 1)

        package = packages[0]
        self.assertEqual(package['id'], 1234567)
        self.assertEqual(package['FacilityNumber'], 'FakeFacilityNumber, LLC')
        self.assertEqual(package['product_name'], 'Cool Product')

        packageRow['product_name'] = 'Cool Product 2.0'
        dbClient.writeRow(packageRow, 'packages', keys=['id'])

        packages = dbClient.query('SELECT * FROM packages WHERE "FacilityNumber"=\'{}\';'.format('FakeFacilityNumber, LLC'))
        packages = list(packages)
        self.assertEqual(len(packages), 1)

        package = packages[0]
        self.assertEqual(package['id'], 1234567)
        self.assertEqual(package['FacilityNumber'], 'FakeFacilityNumber, LLC')
        self.assertEqual(package['product_name'], 'Cool Product 2.0')

    def test_updateRow(self):
        packageRow = {
            'id': 1234567,
            'FacilityNumber': 'FakeFacilityNumber, LLC',
            'product_name': 'Cool Product'
        }

        dbClient.writeRow(packageRow, 'packages')

        packageRow['product_name'] = 'Cool Product 2.0'

        dbClient.updateRow({
            'id': packageRow['id'],
        }, packageRow, 'packages')

        result = dbClient.getRow('packages', {
            'id': packageRow['id'],
        })

        self.assertEqual(result['product_name'], 'Cool Product 2.0')

    def test_beforeAndAfterRowWritten(self):
        # Reset.
        dbClient._rowHooks = []
        
        tableName = 'packages'
        packageRow = {
            'id': 756,
            'FacilityNumber': 'F00',
            'product_name': 'B4R'
        }
        self.i = 0
        
        def before(row):
            self.assertEqual(packageRow, row)
            # Ensure that the data has NOT already been written,
            row = dbClient.getRow(tableName, {'id': packageRow['id']})
            self.assertIsNone(row)
            # Increment our counter.
            self.i += 1
        dbClient.addRowHook(RowHook(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.WRITE, tableName, before))

        def after(row):
            self.assertEqual(packageRow, row)
            # Ensure that `after` is after `before`.
            self.assertEqual(self.i, 1)
        dbClient.addRowHook(RowHook(RowHook.PREPOSITION.AFTER, RowHook.ACTION.WRITE, tableName, after))

        result = dbClient.writeRow(packageRow, tableName)
        self.assertIsNotNone(result.lastrowid)

    def test_getRows(self):
        rows = list(dbClient.getRows('packages', {
            'FacilityNumber': 'FacilityNumber0'    
        }))

        rows.sort(key=lambda x: x['id'])

        self.assertEqual(len(rows), 4)

        self.assertEqual(rows[0]['id'], 1)
        self.assertEqual(rows[0]['FacilityNumber'], 'FacilityNumber0')

        dbClient.getRows('packages', {
            'FacilityNumber': ('LIKE', 'FacilityNumber%')    
        })

    def test_deleteRows(self):
        items = dbClient.deleteRows('packages', {
        'FacilityNumber': 'FacilityNumber0'
        })

        items = dbClient.getRows('packages', {
            'FacilityNumber': 'FacilityNumber0'
        })
        self.assertEqual(len(list(items)), 0)
        
    def test_beforeAndAfterRowDeleted(self):
        dbClient._rowHooks = []
        
        tableName = 'packages'
        packageRow = {
            'id': 1024,
            'FacilityNumber': 'F00',
            'product_name': 'B4R'
        }
        self.i = 0
        
        # We must have a record to delete.
        result = dbClient.writeRow(packageRow, tableName)
        self.assertIsNotNone(result.lastrowid)
        
        def before(row):
            self.assertEqual(packageRow, row)
            # Ensure that the data has NOT already been deleted,
            row = dbClient.getRow(tableName, {'id': packageRow['id']})
            self.assertIsNotNone(row)
            # Increment our counter.
            self.i += 1
        dbClient.addRowHook(RowHook(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.DELETE, tableName, before))

        def after(row):
            self.assertEqual(packageRow, row)
            # Ensure that `after` is after `before`.
            self.assertEqual(self.i, 1)
        dbClient.addRowHook(RowHook(RowHook.PREPOSITION.AFTER, RowHook.ACTION.DELETE, tableName, after))

        dbClient.deleteRows(tableName, {'id': packageRow['id']})


if __name__ == '__main__':
    unittest.main()
