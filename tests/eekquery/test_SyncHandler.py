from eekquery.Constraint import Constraint
from eekquery.MysqlClient import MysqlClient
from eekquery.MysqlConfig import TEST_CONFIG
from eekquery.RowHook import RowHook
from eekquery.SyncHandler import SyncHandler
from eekquery.TrustedStringLiteral import TrustedStringLiteral
import unittest
from unittest.mock import MagicMock

class test_SyncHandler(unittest.TestCase):
    
    def setUp(self):
        global rds, TABLE_NAME

        rds = MysqlClient(config=TEST_CONFIG)
        
        TABLE_NAME = 'Packages'
        
        rds.query('DROP TABLE IF EXISTS {};'.format(TABLE_NAME))
        rds.query('''
            CREATE TABLE {}(Id INT(11) NOT NULL AUTO_INCREMENT, FacilityNumber VARCHAR(45),
            ProductName VARCHAR(45), ItemStatus VARCHAR(45), LastModified TIMESTAMP, PRIMARY KEY (Id));
        '''.format(TABLE_NAME))

    def test_syncAll(self):
        facilityNumber = 'Fake Facility Please Sync Me'
        facility1 = {'FacilityNumber': facilityNumber}
        facilityNumber2 = 'Some other facility'
        facility2 = {'FacilityNumber': facilityNumber2}
 
        intitialRows = [
            {
                'Id': 1,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 1'
            },
            {
                'Id': 2,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 2'
            },
            {
                'Id': 5,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 3'
            },
            {
                'Id': 6,
                'FacilityNumber': facilityNumber2,
                'ProductName': 'Totally Legitimate Product'
            }
        ]
 
        for row in intitialRows:
            rds.writeRow(row, TABLE_NAME)
 
        packages = list(rds.query('SELECT * FROM {};'.format(TABLE_NAME)))
        self.assertEqual(len(packages), 4)
 
        def getUpdatedRows(facility, **kwargs):
            if facility == facility1:
                return [
                    {
                    'Id': 1,
                    'FacilityNumber': facilityNumber,
                    'ProductName': 'Cool Product 1'
                    },
                    # UPDATE #2
                    {
                        'Id': 2,
                        'FacilityNumber': facilityNumber,
                        'ProductName': 'Cool Product 2.0'
                    },
                    # DELETE #5
                    # INSERT #11
                    {
                        'Id': 11,
                        'FacilityNumber': facilityNumber,
                        'ProductName': 'Spiffy Product 9000'
                    }
                ]
            elif facility == facility2:
                # DELETE #6
                return []
         
        sync = SyncHandler(
            writeToTable=TABLE_NAME,
            # Update/delete per-facility.
            deletionConstraints=[
                Constraint(
                    targetColumnName='FacilityNumber'
                ),
            ],
            getRowsForObjectF=getUpdatedRows,
            client=rds
        )
        sync.syncAll([facility1, facility2])
 
        packages = list(rds.query('''
            SELECT * FROM {} WHERE FacilityNumber="{}";
        '''.format(TABLE_NAME, facilityNumber)))
        self.assertEqual(len(packages), 3)
 
        packages.sort(key=lambda p: p['Id'])
 
        self.assertEqual(packages[0]['Id'], 1)
        self.assertEqual(packages[0]['FacilityNumber'], facilityNumber)
        self.assertEqual(packages[0]['ProductName'], 'Cool Product 1')
 
        self.assertEqual(packages[1]['Id'], 2)
        self.assertEqual(packages[1]['FacilityNumber'], facilityNumber)
        self.assertEqual(packages[1]['ProductName'], 'Cool Product 2.0')
 
        sync = SyncHandler(
            writeToTable=TABLE_NAME,
            # Update/delete per-facility.
            deletionConstraints=[
                Constraint(
                    targetColumnName='FacilityNumber'
                ),
            ],
            getRowsForObjectF=getUpdatedRows,
            client=rds
        )
        sync.syncAll([facility1, facility2])

    def test_deleteWhereNotInWithLastModifiedRange(self):
        from datetime import datetime
        
        # Reset listeners.
        rds._rowHooks = []
        
        before = MagicMock()
        rds.addRowHook(RowHook(RowHook.PREPOSITION.BEFORE, RowHook.ACTION.DELETE, TABLE_NAME, before))
        
        after = MagicMock()
        rds.addRowHook(RowHook(RowHook.PREPOSITION.AFTER, RowHook.ACTION.DELETE, TABLE_NAME, after))
        
        facilityNumber = 'Fake Facility Please Sync Me'
        facility1 = {'FacilityNumber': facilityNumber}

        intitialRows = [
            {
                'Id': 1,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 1',
                'LastModified': '2018-01-01 00:00:00'
            },
            {
                'Id': 2,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 2',
                'LastModified': '2018-01-02 12:00:00'
            },
            {
                'Id': 3,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 3',
                'LastModified': '2018-01-02 13:00:00'
            },
            {
                'Id': 4,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 4',
                'LastModified': '2018-01-02 14:00:00'
            },
            {
                'Id': 5,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 5',
                'LastModified': '2018-01-03 00:00:00'
            }
        ]

        for row in intitialRows:
            rds.writeRow(row, TABLE_NAME)

        packages = list(rds.query('SELECT * FROM {};'.format(TABLE_NAME)))
        self.assertEqual(len(packages), 5)
        
        ##
        ## Remove a row inside last modified range.
        ##
        
        rows = [
            {
                'Id': 1,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 1',
                'LastModified': '2018-01-01 00:00:00'
            },
            {
                'Id': 2,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 2',
                'LastModified': '2018-01-02 12:00:00'
            },
            # DELETE 3
            {
                'Id': 4,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 4',
                'LastModified': '2018-01-02 14:00:00'
            },
            {
                'Id': 5,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 5',
                'LastModified': '2018-01-03 00:00:00'
            }
        ]
        
        dateFormat = '%Y-%m-%d %H:%M:%S'
        start = datetime.strptime('2018-01-02 12:00:01', dateFormat)
        end = datetime.strptime('2018-01-02 13:59:59', dateFormat)
        sync = SyncHandler(
            writeToTable=TABLE_NAME,
            # Update/delete per-facility.
            deletionConstraints=[
                Constraint(
                    targetColumnName='FacilityNumber'
                ),
                Constraint(
                    targetColumnName='LastModified',
                    comparisonOperator='BETWEEN',
                    operand=TrustedStringLiteral('"{}" AND "{}"'.format(start, end))
                )
            ],
            getRowsForObjectF=lambda *args, **kwargs: rows,
            client=rds
        )
        sync._deleteWhereNotIn(facility1, rows)
        
        before.assert_called_once()
        after.assert_called_once()
        
        packages = list(rds.query('''
            SELECT * FROM {} WHERE FacilityNumber="{}";
        '''.format(TABLE_NAME, facilityNumber)))
        self.assertEqual(len(packages), 4)

        row = rds.getRow(TABLE_NAME, {'Id': 3})
        self.assertIsNone(row)
        
        
        
        
        # force #2 out of last modified range
        rds.writeRow({
            'Id': 2,
            'FacilityNumber': facilityNumber,
            'ProductName': 'Cool Product 2',
            'LastModified': '2018-01-01 00:00:00'
        }, TABLE_NAME)
        
        rows = [
            {
                'Id': 1,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 1',
                'LastModified': '2018-01-01 00:00:00'
            },
            # SKIP 2
            # DELETE 4
            {
                'Id': 5,
                'FacilityNumber': facilityNumber,
                'ProductName': 'Cool Product 5',
                'LastModified': '2018-01-03 00:00:00'
            }
        ]
        
        dateFormat = '%Y-%m-%d %H:%M:%S'
        start = datetime.strptime('2018-01-02 12:00:00', dateFormat)
        end = datetime.strptime('2018-01-02 14:00:00', dateFormat)
        sync = SyncHandler(
            writeToTable=TABLE_NAME,
            # Update/delete per-facility.
            deletionConstraints=[
                Constraint(
                    targetColumnName='FacilityNumber'
                ),
                Constraint(
                    targetColumnName='LastModified',
                    comparisonOperator='BETWEEN',
                    operand=TrustedStringLiteral('"{}" AND "{}"'.format(start, end))
                )
            ],
            getRowsForObjectF=lambda *args, **kwargs: rows,
            client=rds
        )
        sync._deleteWhereNotIn(facility1, rows)
        
        packages = list(rds.query('''
            SELECT * FROM {} WHERE FacilityNumber="{}";
        '''.format(TABLE_NAME, facilityNumber)))
        self.assertEqual(len(packages), 3)

        row = rds.getRow(TABLE_NAME, {'Id': 2})
        self.assertTrue(row is not None)
        
        row = rds.getRow(TABLE_NAME, {'Id': 4})
        self.assertIsNone(row)
        

if __name__ == '__main__':
    unittest.main()
