from collections import OrderedDict
from eekquery.Constraint import Constraint
from eekquery.TrustedStringLiteral import TrustedStringLiteral
from eekquery.WhereClauseBuilder import WhereClauseBuilder
import unittest

class test_WhereClauseBuilder(unittest.TestCase):

    def test_createWhereClauseFromDictionary(self):
        expected = ' WHERE foo=%s AND a=%s AND LastModified BETWEEN "2000-01-01 00:00:00" AND "2000-01-01 23:59:59"'
        
        dictionary = OrderedDict()
        dictionary['foo'] = 'bar'
        dictionary['a'] = 1
        start = '2000-01-01 00:00:00'
        end = '2000-01-01 23:59:59'
        dictionary['LastModified'] = ('BETWEEN', TrustedStringLiteral('"{}" AND "{}"'.format(start, end)))
        
        builder = WhereClauseBuilder()
        queryParameters = []
        whereClause = builder.createWhereClauseFromDictionary(dictionary, queryParameters)
        
        self.assertEqual(expected, whereClause)
        
        self.assertEquals(2, len(queryParameters))
        self.assertEqual(dictionary['foo'], queryParameters[0])
        self.assertEqual(dictionary['a'], queryParameters[1])

    def test_createWhereClauseFromConstraints(self):
        expected = ' WHERE FacilityNumber=%s AND LastModified BETWEEN "2000-01-01 00:00:00" AND "2000-01-01 23:59:59"'
        
        entity = {
            'Number': 'Foo Facility',
        }
        
        start = '2000-01-01 00:00:00'
        end = '2000-01-01 23:59:59'
        constraints = [
            Constraint(
                targetColumnName='FacilityNumber',
                sourceKeyName='Number'
            ),
            Constraint(
                targetColumnName='LastModified',
                comparisonOperator='BETWEEN',
                operand=TrustedStringLiteral('"{}" AND "{}"'.format(start, end))
            )
        ]
        
        builder = WhereClauseBuilder()
        queryParameters = []
        whereClause = builder.createWhereClauseFromConstraints(constraints, queryParameters, entity=entity)
        
        self.assertEqual(expected, whereClause)
        
        self.assertEquals(1, len(queryParameters))
        self.assertEqual('Foo Facility', queryParameters[0])

if __name__ == '__main__':
    unittest.main()
