from eekquery.DataObject import DataObject
import unittest

class test_DataObject(unittest.TestCase):
    
    def test_assignment(self):
        obj = DataObject({
            'foo': 5,
            'bar': [],
        }, autoField=True)

        obj.foo = 10
        bar = [1, 2, 3]
        obj.bar = bar

        self.assertEqual(obj.foo, 10)
        self.assertEqual(obj.bar, bar)

        row = obj.asDict()

        self.assertEqual(row['foo'], 10)
        self.assertEqual(row['bar'], bar)

    def test_asTableRow(self):
        class Row(DataObject):
            def __init__(self, row):
                super().__init__(row)

                self.forTable('Fake Table')
                self.addField('foo')
                self.addField('bar')
                self.addField('baz')
                self.addField('buzz')


        r = Row({
            'foo': 1,
            'bar': 2,
        })

        boo = Row({
            'foo': 'potato',
        })

        r.baz = 3
        r.buzz = 4

        result = r.asTableRow()

        self.assertEqual(r.foo, 1)
        self.assertEqual(r.bar, 2)

        self.assertEqual(result['foo'], 1)
        self.assertEqual(result['bar'], 2)
        self.assertEqual(result['baz'], 3)
        self.assertEqual(result['buzz'], 4)

if __name__ == '__main__':
    unittest.main()
