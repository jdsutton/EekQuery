from eekquery.RequiresPermission import RequiresPermission
import unittest

class test_RequiresPermission(unittest.TestCase):
    
    def test(self):
        def permssionF(x):
            return x < 10

        @RequiresPermission(permssionF)
        def foo(x):
            return 'bar'

        foo(9)

        def noPermission():
            foo(11)

        self.assertRaises(RequiresPermission.PermissionException, noPermission)

if __name__ == '__main__':
    unittest.main()