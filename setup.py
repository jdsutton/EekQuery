#!/usr/bin/env python3

from setuptools import setup, find_packages
from setuptools.command.install import install
from subprocess import check_call as execute

repoDependencies = [
    'git+https://gitlab.com/jdsutton/py3typing',
]

class PostEekQueryInstall(install):
    def run(self):
        for repo in repoDependencies:
            execute('pip3 install {}'.format(repo), shell=True)
        install.run(self)

setup(name='EekQuery',
    version='1.0',
    description='',
    author='Eek, Inc.',
    author_email='',
    url='',
    packages=find_packages(),
    install_requires=[
        'PyMySQL',
        'nose',
        'coverage',
        'sphinx',
        'iso8601',
    ],
    cmdclass={
        'install': PostEekQueryInstall
    }
)
