# EekQuery
MySQL query tools.

## Documentation
https://jdsutton.gitlab.io/EekQuery/

## Installation
`$ make install`

## Running the tests
```
$ export MYSQL_PASSWORD=<your password for root mysql user>
$ make test
```

## Example Usage
```
from eekquery.MysqlClient import MysqlClient
from eekquery.MysqlConfig import MysqlConfig

client = MysqlClient(config=MysqlConfig(...))

# SELECT * FROM myTable
# WHERE MyColum1='Value1' AND MyColumn2 LIKE 'X%'
# LIMIT 100 OFFSET 5 ORDER BY MyColumn2 DESC;
rows = client.getRows('myTable', {
    'MyColumn1': 'Value1',
    'MyColumn2': ('LIKE', "X%")
}, limit=100, offset=5, orderBy="MyColumn2 DESC")
```