#!/bin/bash

# Usage: executePostgreSQL.sh <user> <database> <sql file>

$(which sudo) su postgres <<< "psql $2
$(<$3)"
