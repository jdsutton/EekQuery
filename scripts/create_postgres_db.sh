#!/bin/bash

DATABASE=$1

if [ -z "$1" ]
then
	echo "Must provide a DB name as first argument."
	exit
fi

echo "Creating a new PostgreSQL database: $1"

function yes_or_no {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) return 0  ;;
            [Nn]*) echo "Aborted" ; return  1 ;;
        esac
    done
}

touch ~/.pgpass

yes_or_no "Init new ~/.pgpass file?" && echo "*:5432:$1:root:root" > ~/.pgpass || echo "Make sure your ~/.pgpass file is configured"

chmod 0600 ~/.pgpass
export PGPASSFILE=~/.pgpass

$(which sudo) su postgres <<< "psql
CREATE USER root;
ALTER ROLE root WITH PASSWORD 'root';
ALTER USER root CREATEDB;
CREATE DATABASE $DATABASE;
ALTER DATABASE $DATABASE OWNER TO root;
GRANT ALL PRIVILEGES ON DATABASE $DATABASE TO root;
"

echo "Update your pg_hba.conf to use md5 auth and run: sudo service postgresql restart"
locate pg_hba.conf

# $(which sudo) su root <<< "psql postgres
# CREATE DATABASE $1
# SET timezone=0;
# exit;
# "
